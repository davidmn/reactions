﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotaoAvancar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Avancar()
    {

        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();

        // Retrieve the name of this scene.
        string sceneName = currentScene.name;


        if (sceneName == "Tela1Jogadores")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tela2Assuntos");
        }

        if (sceneName == "Tela2Assuntos")
        {

            UnityEngine.SceneManagement.SceneManager.LoadScene("TelaJogo");
        }

    }
}

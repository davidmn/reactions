﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AvançarTelaLinguagens : MonoBehaviour
{

    void Awake()
    {
        
        if (PlayerPrefs.HasKey("lang"))
        {
            switch (PlayerPrefs.GetInt("lang"))
            {
                case 0:
                    GlobalVar.Language = 0;
                    GlobalVar.LanguageString = "pt";
                    break;
                case 1:
                    GlobalVar.Language = 1;
                    GlobalVar.LanguageString = "en";
                    break;
                case 2:
                    GlobalVar.Language = 2;
                    GlobalVar.LanguageString = "es";
                    break;
                case 3:
                    GlobalVar.Language = 3;
                    GlobalVar.LanguageString = "fr";
                    break;
                default:
                    GlobalVar.Language = 0;
                    GlobalVar.LanguageString = "pt";
                    break;
            }
            SceneManager.LoadScene("TelaInicial");
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("lang"))
        {
            switch (PlayerPrefs.GetInt("lang"))
            {
                case 0:
                    GlobalVar.Language = 0;
                    GlobalVar.LanguageString = "pt";
                    break;
                case 1:
                    GlobalVar.Language = 1;
                    GlobalVar.LanguageString = "en";
                    break;
                case 2:
                    GlobalVar.Language = 2;
                    GlobalVar.LanguageString = "es";
                    break;
                case 3:
                    GlobalVar.Language = 3;
                    GlobalVar.LanguageString = "fr";
                    break;
                default:
                    GlobalVar.Language = 0;
                    GlobalVar.LanguageString = "pt";
                    break;
            }
            SceneManager.LoadScene("TelaInicial");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

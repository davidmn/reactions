﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotoesJogadoresAvancar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Button>().interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GlobalVar.NumJogadores != 0)
        {
            this.gameObject.GetComponent<Button>().interactable = true;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotaoVoltar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Voltar();
            Carta.cliques = 0;
        }

    }

    public void Voltar()
    {
        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();

        // Retrieve the name of this scene.
        string sceneName = currentScene.name;

        if (sceneName == "Tela1Jogadores")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("TelaInicial");
        }

        if (sceneName == "Tela2Assuntos")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tela1Jogadores");
        }

        if (sceneName == "TelaJogo")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("TelaInicial");
            Carta.cliques = 0;
        }

    }



}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBotoes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonPlay()
    {
        Debug.Log("Indo para tela de seleção de jogadores");
        SceneManager.LoadScene("Tela1Jogadores");
    }

    public void ChangeLanguage()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                PlayerPrefs.SetInt("lang", 0);
                GlobalVar.Language = 1;
                GlobalVar.LanguageString = "en";                
                break;
            case 1:
                PlayerPrefs.SetInt("lang", 1);
                GlobalVar.Language = 2;
                GlobalVar.LanguageString = "es";
                break;
            case 2:
                PlayerPrefs.SetInt("lang", 2);
                GlobalVar.Language = 3;
                GlobalVar.LanguageString = "fr";
                break;
            case 3:
                PlayerPrefs.SetInt("lang", 3);
                GlobalVar.Language = 0;
                GlobalVar.LanguageString = "pt";
                break;
            default:
                PlayerPrefs.SetInt("lang", 0);
                GlobalVar.Language = 0;
                GlobalVar.LanguageString = "pt";
                break;
        }
        Debug.Log("A lingua atual é: " + GlobalVar.LanguageString);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void ButtonImprimir()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                Application.OpenURL("http://www.ldse.ufc.br/reactions_por.pdf");
                break;
            case 1:
                Application.OpenURL("http://www.ldse.ufc.br/reactions_eng.pdf");
                break;
            case 2:
                Application.OpenURL("http://www.ldse.ufc.br/reactions_spa.pdf");
                break;
            case 3:
                Application.OpenURL("http://www.ldse.ufc.br/reactions_fre.pdf");
                break;
            default:
                Application.OpenURL("http://www.ldse.ufc.br/reactions_por.pdf");
                break;
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraducaoBotoes : MonoBehaviour
{
    public Sprite BotaoNormal_pt, BotaoPressionado_pt, BotaoNormal_en, BotaoPressionado_en, BotaoNormal_es, BotaoPressionado_es, BotaoNormal_fr, BotaoPressionado_fr;

    // Start is called before the first frame update
    void Start()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                GetComponent<Image>().overrideSprite = BotaoNormal_pt;
                //SpriteState spriteState1 = new SpriteState();
                //spriteState1 = GetComponent<Button>().spriteState;
                //spriteState1.pressedSprite = BotaoPressionado_pt;                
                break;
            case 1:
                GetComponent<Image>().overrideSprite = BotaoNormal_en;
                //SpriteState spriteState2 = new SpriteState();
                //spriteState2 = GetComponent<Button>().spriteState;
                //spriteState2.pressedSprite = BotaoPressionado_en;
                break;
            case 2:
                GetComponent<Image>().overrideSprite = BotaoNormal_es;
                //SpriteState spriteState3 = new SpriteState();
                //spriteState3 = GetComponent<Button>().spriteState;
                //spriteState3.pressedSprite = BotaoPressionado_es;
                break;
            case 3:
                GetComponent<Image>().overrideSprite = BotaoNormal_fr;
                //SpriteState spriteState4 = new SpriteState();
                //spriteState4 = GetComponent<Button>().spriteState;
                //spriteState4.pressedSprite = BotaoPressionado_fr;
                break;
            default:
                break;
        }     
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

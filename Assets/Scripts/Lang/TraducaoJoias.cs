﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraducaoJoias : MonoBehaviour
{
    public Sprite CartaJoia_pt, CartaJoia_en, CartaJoia_es, CartaJoia_fr;
    public GameObject CardJoia;
    // Start is called before the first frame update
    void Start()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                CardJoia.GetComponent<Image>().overrideSprite = CartaJoia_pt;                
                break;
            case 1:
                CardJoia.GetComponent<Image>().overrideSprite = CartaJoia_en;              
                break;
            case 2:
                CardJoia.GetComponent<Image>().overrideSprite = CartaJoia_es;       
                break;
            case 3:
                CardJoia.GetComponent<Image>().overrideSprite = CartaJoia_fr;  
                break;
            default:
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

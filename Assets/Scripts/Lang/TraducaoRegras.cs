﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraducaoRegras : MonoBehaviour
{
    public Sprite Regra_pt, Regra_en, Regra_es, Regra_fr;
    // Start is called before the first frame update
    void Start()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                GetComponent<Image>().overrideSprite = Regra_pt;
                break;
            case 1:
                GetComponent<Image>().overrideSprite = Regra_en;
                break;
            case 2:
                GetComponent<Image>().overrideSprite = Regra_es;
                break;
            case 3:
                GetComponent<Image>().overrideSprite = Regra_fr;
                break;
            default:
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

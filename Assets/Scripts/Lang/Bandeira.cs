﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bandeira : MonoBehaviour
{
    Sprite spritebandeira;

    Texture2D tex;

    // Start is called before the first frame update
    void Start()
    {
        switch (GlobalVar.Language)
        {

            case 0:
                tex = Resources.Load<Texture2D>("Img/01_Geral/portugues_3");

                spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                break;
            case 1:
                tex = Resources.Load<Texture2D>("Img/01_Geral/english_3");

                spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                break;
            case 2:
                tex = Resources.Load<Texture2D>("Img/01_Geral/espanol_3");

                spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                break;
            case 3:
                tex = Resources.Load<Texture2D>("Img/01_Geral/francais_3");

                spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                break;
            default:
                tex = Resources.Load<Texture2D>("Img/01_Geral/english_3");

                spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                break;


        }
        GetComponent<Image>().sprite = spritebandeira;

    }

    // Update is called once per frame
    void Update()
    {
        //switch (GlobalVar.Language)
        //{

        //    case 0:
        //        tex = Resources.Load<Texture2D>("Img/01_Geral/flag_pt_3");

        //        spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        //        break;
        //    case 1:
        //        tex = Resources.Load<Texture2D>("Img/01_Geral/flag_en_3");

        //        spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        //        break;
        //    case 2:
        //        tex = Resources.Load<Texture2D>("Img/01_Geral/flag_es_3");

        //        spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        //        break;
        //    case 3:
        //        tex = Resources.Load<Texture2D>("Img/01_Geral/flag_fr_3");

        //        spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        //        break;
        //    default:
        //        tex = Resources.Load<Texture2D>("Img/01_Geral/flag_en_3");

        //        spritebandeira = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        //        break;


        //}
        //GetComponent<Image>().sprite = spritebandeira;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotaoResposta : MonoBehaviour
{
    public GameObject CartaMoldura, titulo, BotaoDados, BotaoProxCarta;
    public Sprite CardTrue, CardFalse;    
    public Sprite TituloTrue_pt, TituloTrue_en, TituloTrue_es, TituloTrue_fr, TituloFalse_pt, TituloFalse_en, TituloFalse_es, TituloFalse_fr;
    Sprite TituloTrue, TituloFalse;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickBotaoResposta()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                TituloTrue = TituloTrue_pt;
                TituloFalse = TituloFalse_pt;
                break;
            case 1:
                TituloTrue = TituloTrue_en;
                TituloFalse = TituloFalse_en;
                break;
            case 2:
                TituloTrue = TituloTrue_es;
                TituloFalse = TituloFalse_es;
                break;
            case 3:
                TituloTrue = TituloTrue_fr;
                TituloFalse = TituloFalse_fr;
                break;
            default:
                TituloTrue = TituloTrue_pt;
                TituloFalse = TituloFalse_pt;
                break;
        }



        int NumeroCartaTemp = Carta.NumeroCarta + 1;

        if (NumeroCartaTemp % 2 != 0)
        {
            Debug.Log("A carta " + NumeroCartaTemp + " é verdadeira");
            titulo.GetComponent<Image>().overrideSprite = TituloTrue;
            CartaMoldura.GetComponent<Image>().overrideSprite = CardTrue;
        }
        else
        {
            Debug.Log("A carta " + NumeroCartaTemp + " é falsa");
            titulo.GetComponent<Image>().overrideSprite = TituloFalse;
            CartaMoldura.GetComponent<Image>().overrideSprite = CardFalse;
        }
        CartaMoldura.SetActive(true);

        BotaoDados.SetActive(true);
        BotaoProxCarta.SetActive(true);
        Carta.cliques++;
        gameObject.SetActive(false);



    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalVar : MonoBehaviour
{
    public static int NumJogadores = 0; //Valor do número de jogadores 
    public static int[] Assuntos = { 0, 0, 0, 0, 0, 0 }; //Cada posição um assunto. Valor 0 para assunto inativo e valor 1 para assunto ativado
    public static int Language = 0; //0 = PT, 1 = EN, 2 = ES, 3 = FR
    public static string LanguageString = "pt";

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(PlayerPrefs.HasKey("lang"));
        Debug.Log(PlayerPrefs.GetInt("lang"));
    }

    // Update is called once per frame
    void Update()
    {
        SairDoJogo();
    }


    void SairDoJogo()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public void BotaoSairDoJogo()
    {
        Application.Quit();
    }

    public void LinguaPt()
    {
        PlayerPrefs.SetInt("lang", 0);
        Language = 0;
        LanguageString = "pt";
        SceneManager.LoadScene("TelaInicial");
    }

    public void LinguaEn()
    {
        PlayerPrefs.SetInt("lang", 1);
        Language = 1;
        LanguageString = "en";
        SceneManager.LoadScene("TelaInicial");
    }

    public void LinguaEs()
    {
        PlayerPrefs.SetInt("lang", 2);
        Language = 2;
        LanguageString = "es";
        SceneManager.LoadScene("TelaInicial");
    }

    public void LinguaFr()
    {
        PlayerPrefs.SetInt("lang", 3);
        Language = 3;
        LanguageString = "fr";
        SceneManager.LoadScene("TelaInicial");
    }
}

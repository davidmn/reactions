﻿    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotoesAssuntosAvancar : MonoBehaviour
{
    

    public Sprite BotaoNormal_pt, BotaoDesabilitado_pt, BotaoNormal_en, BotaoDesabilitado_en, BotaoNormal_es, BotaoDesabilitado_es, BotaoNormal_fr, BotaoDesabilitado_fr;
    Sprite image_disable, image_enable;

    void Start()
    {
        switch (GlobalVar.Language)
        {
            case 0:
                image_enable = BotaoNormal_pt;
                image_disable = BotaoDesabilitado_pt;
                break;
            case 1:
                image_enable = BotaoNormal_en;
                image_disable = BotaoDesabilitado_en;
                break;
            case 2:
                image_enable = BotaoNormal_es;
                image_disable = BotaoDesabilitado_es;
                break;
            case 3:
                image_enable = BotaoNormal_fr;
                image_disable = BotaoDesabilitado_fr;
                break;
            default:
                image_enable = BotaoNormal_pt;
                image_disable = BotaoDesabilitado_pt;
                break;
        }


        this.gameObject.GetComponent<Button>().interactable = false;
        GetComponent<Image>().overrideSprite = image_disable;

    }

    void Update()
    {
        
        int pos = Array.IndexOf(GlobalVar.Assuntos, 1);
        if (pos > -1)
        {            
            // the array contains the string and the pos variable
            // will have its position in the array
            this.gameObject.GetComponent<Button>().interactable = true;
            GetComponent<Image>().overrideSprite = image_enable;
        }
        else
        {
            this.gameObject.GetComponent<Button>().interactable = false;
            GetComponent<Image>().overrideSprite = image_disable;
        }
    }
}

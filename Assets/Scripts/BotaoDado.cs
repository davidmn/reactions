﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BotaoDado : MonoBehaviour
{
    public GameObject SpriteDado1, TextoDado1, SpriteDado2, TextoDado2, SpriteDado3, TextoDado3, SpriteDado4, TextoDado4, SpriteDado5, TextoDado5, SpriteDado6, TextoDado6;
    public GameObject[,] objects;
    public int ValorDado1, ValorDado2, ValorDado3, ValorDado4, ValorDado5, ValorDado6;
    public static int[] valores = { 1, 2, 3, 4, 5, 6 };


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 5; i > -1; i--)
        {
            int j = UnityEngine.Random.Range(0, valores.Length);
            int temp = valores[j];
            valores[j] = valores[i];
            valores[i] = temp;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void JogarDados()
    {
        
        for (int i = 0; i<6; i++)
        {
            Debug.Log("A posição " + i + " tem valor: " + valores[i]);
        }

        TextoDado1.GetComponent<TextMeshProUGUI>().text = "0" + valores[0];
        TextoDado2.GetComponent<TextMeshProUGUI>().text = "0" + valores[1];
        TextoDado3.GetComponent<TextMeshProUGUI>().text = "0" + valores[2];
        TextoDado4.GetComponent<TextMeshProUGUI>().text = "0" + valores[3];
        TextoDado5.GetComponent<TextMeshProUGUI>().text = "0" + valores[4];
        TextoDado6.GetComponent<TextMeshProUGUI>().text = "0" + valores[5];

        switch (GlobalVar.NumJogadores)
        {
            case 2:
                SpriteDado1.SetActive(true);
                SpriteDado2.SetActive(true);
                break;
            case 3:
                SpriteDado1.SetActive(true);
                SpriteDado2.SetActive(true);
                SpriteDado3.SetActive(true);
                break;
            case 4:
                SpriteDado1.SetActive(true);
                SpriteDado2.SetActive(true);
                SpriteDado3.SetActive(true);
                SpriteDado4.SetActive(true);
                break;
            case 5:
                SpriteDado1.SetActive(true);
                SpriteDado2.SetActive(true);
                SpriteDado3.SetActive(true);
                SpriteDado4.SetActive(true);
                SpriteDado5.SetActive(true);
                break;
            case 6:
                SpriteDado1.SetActive(true);
                SpriteDado2.SetActive(true);
                SpriteDado3.SetActive(true);
                SpriteDado4.SetActive(true);
                SpriteDado5.SetActive(true);
                SpriteDado6.SetActive(true);
                break;                
            default:
                break;
        }

    }



}

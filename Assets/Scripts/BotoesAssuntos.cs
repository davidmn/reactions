﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotoesAssuntos : MonoBehaviour
{
    public GameObject ValorAssunto;
    public Sprite image_on_pt, image_on_en, image_on_es, image_on_fr;
    public Sprite image_off_pt, image_off_en, image_off_es, image_off_fr;
    Sprite image_on, image_off;
    // Start is called before the first frame update
    void Start()
    {
        //Definir os sprites com as linguas certas
        switch (GlobalVar.Language)
        {
            case 0:
                image_on = image_on_pt;
                image_off = image_off_pt;
                break;
            case 1:
                image_on = image_on_en;
                image_off = image_off_en;
                break;
            case 2:
                image_on = image_on_es;
                image_off = image_off_es;
                break;
            case 3:
                image_on = image_on_fr;
                image_off = image_off_fr;
                break;
            default:
                break;
        }


        switch (ValorAssunto.name)
        {
            case "Assunto 1":
                if (GlobalVar.Assuntos[0] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                }
                break;
            case "Assunto 2":
                if (GlobalVar.Assuntos[1] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                }
                break;
            case "Assunto 3":
                if (GlobalVar.Assuntos[2] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                }
                break;
            case "Assunto 4":
                if (GlobalVar.Assuntos[3] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                }
                break;
            case "Assunto 5":
                if (GlobalVar.Assuntos[4] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                }
                break;
            case "Assunto 6":
                if (GlobalVar.Assuntos[5] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                }
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelecionarAssunto()
    {
        Debug.Log(ValorAssunto.name);
        switch (ValorAssunto.name)
        {
            case "Assunto 1":
                if (GlobalVar.Assuntos[0] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                    GlobalVar.Assuntos[0] = 1;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;
                    GlobalVar.Assuntos[0] = 0;
                }
                break;
            case "Assunto 2":
                if (GlobalVar.Assuntos[1] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                    GlobalVar.Assuntos[1] = 1;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;
                    GlobalVar.Assuntos[1] = 0;
                }
                break;
            case "Assunto 3":
                if (GlobalVar.Assuntos[2] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                    GlobalVar.Assuntos[2] = 1;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;
                    GlobalVar.Assuntos[2] = 0;
                }
                break;
            case "Assunto 4":
                if (GlobalVar.Assuntos[3] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                    GlobalVar.Assuntos[3] = 1;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;
                    GlobalVar.Assuntos[3] = 0;
                }
                break;
            case "Assunto 5":
                if (GlobalVar.Assuntos[4] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                    GlobalVar.Assuntos[4] = 1;

                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;
                    GlobalVar.Assuntos[4] = 0;
                }
                break;
            case "Assunto 6":
                if (GlobalVar.Assuntos[5] == 0)
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_on;
                    GlobalVar.Assuntos[5] = 1;
                }
                else
                {
                    ValorAssunto.GetComponent<Image>().overrideSprite = image_off;
                    GlobalVar.Assuntos[5] = 0;
                }
                break;
            default:
                break;
        }
    }
}
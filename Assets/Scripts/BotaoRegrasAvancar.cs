﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotaoRegrasAvancar : MonoBehaviour
{
    public GameObject Regras01, Regras02, Regras03, Regras04, Regras05, Regras06, Regras07, CanvasRegras, BotaoAvancar, BotaoRetroceder;
    public static int TelaRegraAtual;
    GameObject[] objetos = new GameObject[7];

    // Start is called before the first frame update
    void Start()
    {
        TelaRegraAtual = 0;
        objetos = new GameObject[] { Regras01, Regras02, Regras03, Regras04, Regras05, Regras06, Regras07 };
        BotaoRetroceder.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Avancar()
    {
        TelaRegraAtual++;
        for (int i = 0; i < 7; i++)
        {
            if (i == TelaRegraAtual)
            {
                objetos[i].SetActive(true);
            }
            else
            {
                objetos[i].SetActive(false);
            }
        }

        //desabilita o botao caso chegue no valor maximo
        if (TelaRegraAtual == 6)
        {
            BotaoAvancar.SetActive(false);
        }

        if (TelaRegraAtual == 1)
        {
            BotaoRetroceder.SetActive(true);
        }
    }

    public void Retroceder()
    {
        if (TelaRegraAtual == 0)
        {

        }
        else
        {
            TelaRegraAtual--;
            for (int i = 0; i < 7; i++)
            {
                if (i == TelaRegraAtual)
                {
                    objetos[i].SetActive(true);
                }
                else
                {
                    objetos[i].SetActive(false);
                }
            }

            if (TelaRegraAtual == 5)
            {
                BotaoAvancar.SetActive(true);
            }

            if (TelaRegraAtual == 0)
            {
                BotaoRetroceder.SetActive(false);
            }
        }
    }

    //fecha o canvas e reinicia os valores
    public void FecharCanvas()
    {
        
        BotaoRetroceder.SetActive(false);
        BotaoAvancar.SetActive(true);
        CanvasRegras.SetActive(false);
        TelaRegraAtual = 0;
        for (int i = 0; i < 7; i++)
        {
            if (i == TelaRegraAtual)
            {
                objetos[i].SetActive(true);
            }
            else
            {
                objetos[i].SetActive(false);
            }
        }

    }
}

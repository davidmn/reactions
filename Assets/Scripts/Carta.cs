﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Carta : MonoBehaviour
{
    public Sprite CardAssunto1_pt, CardAssunto1_en, CardAssunto1_es, CardAssunto1_fr, CardAssunto2_pt, CardAssunto2_en, CardAssunto2_es, CardAssunto2_fr, CardAssunto3_pt, CardAssunto3_en, CardAssunto3_es, CardAssunto3_fr, CardAssunto4_pt, CardAssunto4_en, CardAssunto4_es, CardAssunto4_fr, CardAssunto5_pt, CardAssunto5_en, CardAssunto5_es, CardAssunto5_fr, CardAssunto6_pt, CardAssunto6_en, CardAssunto6_es, CardAssunto6_fr, CardTrue, CardFalse, Titulo2_pt, Titulo2_en, Titulo2_es, Titulo2_fr, TituloTrue_pt, TituloTrue_en, TituloTrue_es, TituloTrue_fr, TituloFalse_pt, TituloFalse_en, TituloFalse_es, TituloFalse_fr;
    Sprite CardAssunto1, CardAssunto2, CardAssunto3, CardAssunto4, CardAssunto5, CardAssunto6, Titulo2, TituloTrue, TituloFalse;
    public GameObject titulo, CartaMoldura, TextoCarta, BotaoDados, ImagemLigacaoQuimica, BotaoResposta;
    public List<int> Cartas = new List<int>();
    public System.Random rnd = new System.Random();
    public static int cliques = 0;
    public static int NumeroCarta;

    // Start is called before the first frame update
    void Start()
    {
        if (Camera.main.aspect >= 0.56)
        {
            Debug.Log("9:16 = 0,5625");
            transform.localScale = new Vector3(1.3f, 1.3f, 1.0f);
        }
        else if (Camera.main.aspect >= 0.49)
        {
            Debug.Log("9:18 = 0,5");
            transform.localScale = new Vector3(1.2f, 1.2f, 1.0f);
        }
        else
        {
            Debug.Log("iphone resolution =  0,4620");
        }


        //Definir os sprites com as linguas certas
        switch (GlobalVar.Language)
        {
            case 0:
                CardAssunto1 = CardAssunto1_pt;
                CardAssunto2 = CardAssunto2_pt;
                CardAssunto3 = CardAssunto3_pt;
                CardAssunto4 = CardAssunto4_pt;
                CardAssunto5 = CardAssunto5_pt;
                CardAssunto6 = CardAssunto6_pt;
                break;
            case 1:
                CardAssunto1 = CardAssunto1_en;
                CardAssunto2 = CardAssunto2_en;
                CardAssunto3 = CardAssunto3_en;
                CardAssunto4 = CardAssunto4_en;
                CardAssunto5 = CardAssunto5_en;
                CardAssunto6 = CardAssunto6_en;
                break;
            case 2:
                CardAssunto1 = CardAssunto1_es;
                CardAssunto2 = CardAssunto2_es;
                CardAssunto3 = CardAssunto3_es;
                CardAssunto4 = CardAssunto4_es;
                CardAssunto5 = CardAssunto5_es;
                CardAssunto6 = CardAssunto6_es;
                break;
            case 3:
                CardAssunto1 = CardAssunto1_fr;
                CardAssunto2 = CardAssunto2_fr;
                CardAssunto3 = CardAssunto3_fr;
                CardAssunto4 = CardAssunto4_fr;
                CardAssunto5 = CardAssunto5_fr;
                CardAssunto6 = CardAssunto6_fr;
                break;

            default:
                CardAssunto1 = CardAssunto1_pt;
                CardAssunto2 = CardAssunto2_pt;
                CardAssunto3 = CardAssunto3_pt;
                CardAssunto4 = CardAssunto4_pt;
                CardAssunto5 = CardAssunto5_pt;
                CardAssunto6 = CardAssunto6_pt;
                break;
        }
                          
        for (int i = 0; i < 6; i++)
        {
            if (GlobalVar.Assuntos[i] == 1)
            {
                Cartas.Add(i);
                //Debug.Log(i);
            }
        }



    }

    // Update is called once per frame
    void Update()
    {
        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 5);
    }


    public string ReadStringDeck(int ValorDeck, int ValorCarta, string lingua)
    {
        int ValorCartaMais1 = ValorCarta + 1; //corrigindo o valor do resultado do randomizador pra o presente no txt
        // ANTIGO // int ValorCartaMais2 = ValorCarta + 2;
        string ValorCartaCorreto1 = ValorCartaMais1.ToString("000");// transforma o int ValorCarta em uma string de 3 dígitos com possíveis zeros na frente
        // ANTIGO // string ValorCartaCorreto2 = ValorCartaMais2.ToString("000");// transforma o int ValorCarta em uma string de 3 dígitos com possíveis zeros na frente

        //Obtém o txt inteiro numa única string
        //string path = "Assets/Resources/Txt/deck_0" + (ValorDeck + 1) + "_" + lingua + ".txt";
        //Read the text from directly from the test.txt file
        TextAsset reader = Resources.Load<TextAsset>("Txt/deck_0" + (ValorDeck + 1) + "_" + lingua + "_html");// new StreamReader(path);
        string TextoDoDeck = reader.ToString();
        //reader.Close();

        //Corta a string deixando somente os caracteres da pergunta
        string toFind1 = "A00" + (ValorDeck + 1) + "-" + ValorCartaCorreto1 + ":";
        Debug.Log("Código da carta: " + toFind1);
        // ANTIGO // string toFind2 = "A00" + (ValorDeck + 1) + "-" + ValorCartaCorreto2;
        // ANTIGO // Debug.Log(toFind2);
        int start = TextoDoDeck.IndexOf(toFind1) + toFind1.Length;
        // ANTIGO // int end = TextoDoDeck.IndexOf(toFind2, start); //Start after the index of 'my' since 'is' appears twice
        int end = TextoDoDeck.IndexOf("<p>", start); //Start after the index of 'my' since 'is' appears twice
        string StringPronta = TextoDoDeck.Substring(start, end - start);

        return StringPronta;
    }

    public void RevelarCarta()
    {
        //Verifica a linguagem para definir os sprites do titulo da tela
        switch (GlobalVar.Language)
        {
            case 0:
                Titulo2 = Titulo2_pt;
                TituloTrue = TituloTrue_pt;
                TituloFalse = TituloFalse_pt;
                break;
            case 1:
                Titulo2 = Titulo2_en;
                TituloTrue = TituloTrue_en;
                TituloFalse = TituloFalse_en;
                break;
            case 2:
                Titulo2 = Titulo2_es;
                TituloTrue = TituloTrue_es;
                TituloFalse = TituloFalse_es;
                break;
            case 3:
                Titulo2 = Titulo2_fr;
                TituloTrue = TituloTrue_fr;
                TituloFalse = TituloFalse_fr;
                break;
            default:
                Titulo2 = Titulo2_pt;
                TituloTrue = TituloTrue_pt;
                TituloFalse = TituloFalse_pt;
                break;
        }



        if (cliques == 0)
        {
            int ValorAssunto = rnd.Next(Cartas.Count); //define qual tema será sorteado
            NumeroCarta = UnityEngine.Random.Range(0, 100); //define qual carta do deck vai ser escolhida
            //NumeroCarta = 38; //Altere aqui para verificar uma carta específica
            string pergunta;

            switch (Cartas[ValorAssunto])
            {
                case 0:
                    GetComponent<Image>().overrideSprite = CardAssunto1; //Mostra Sprite do deck sorteado
                    pergunta = ReadStringDeck(Cartas[ValorAssunto], NumeroCarta, GlobalVar.LanguageString); //envia a questão selecionada para uma string
                    TextoCarta.GetComponent<TextMeshProUGUI>().text = pergunta; //escreve o texto da carta para o objeto que exibe o texto
                    TextoCarta.SetActive(true);
                    break;
                case 1:
                    GetComponent<Image>().overrideSprite = CardAssunto2; //Mostra Sprite do deck sorteado
                    pergunta = ReadStringDeck(Cartas[ValorAssunto], NumeroCarta, GlobalVar.LanguageString); //envia a questão selecionada para uma string
                    TextoCarta.GetComponent<TextMeshProUGUI>().text = pergunta; //escreve o texto da carta para o objeto que exibe o texto
                    TextoCarta.SetActive(true);

                    //funções para exibir a ligação química a questão (se houver) (somente para esse deck)
                    int NumeroCartaTemp = NumeroCarta + 1;
                    Texture2D texTemp = Resources.Load<Texture2D>("Img/01_Geral/Deck_02_img/question " + NumeroCartaTemp);
                    
                    if (texTemp != null)
                    {
                        ImagemLigacaoQuimica.SetActive(true);
                        Sprite SpriteLigacaoQuimica = Sprite.Create(texTemp, new Rect(0.0f, 0.0f, texTemp.width, texTemp.height), new Vector2(0.5f, 0.5f), 100.0f);
                        ImagemLigacaoQuimica.GetComponent<Image>().sprite = SpriteLigacaoQuimica;

                        Debug.Log("Imagem Ligacao Quimica existe: Carta " + NumeroCartaTemp);
                    }
                    else
                    {
                        Debug.Log("Imagem Ligacao Quimica NÃO existe: Carta " + NumeroCartaTemp);
                    }

                    break;
                case 2:
                    GetComponent<Image>().overrideSprite = CardAssunto3; //Mostra Sprite do deck sorteado
                    pergunta = ReadStringDeck(Cartas[ValorAssunto], NumeroCarta, GlobalVar.LanguageString); //envia a questão selecionada para uma string
                    TextoCarta.GetComponent<TextMeshProUGUI>().text = pergunta; //escreve o texto da carta para o objeto que exibe o texto
                    TextoCarta.SetActive(true);
                    break;
                case 3:
                    GetComponent<Image>().overrideSprite = CardAssunto4; //Mostra Sprite do deck sorteado
                    pergunta = ReadStringDeck(Cartas[ValorAssunto], NumeroCarta, GlobalVar.LanguageString); //envia a questão selecionada para uma string
                    TextoCarta.GetComponent<TextMeshProUGUI>().text = pergunta; //escreve o texto da carta para o objeto que exibe o texto
                    TextoCarta.SetActive(true);
                    break;
                case 4:
                    GetComponent<Image>().overrideSprite = CardAssunto5; //Mostra Sprite do deck sorteado
                    pergunta = ReadStringDeck(Cartas[ValorAssunto], NumeroCarta, GlobalVar.LanguageString); //envia a questão selecionada para uma string
                    TextoCarta.GetComponent<TextMeshProUGUI>().text = pergunta; //escreve o texto da carta para o objeto que exibe o texto
                    TextoCarta.SetActive(true);
                    break;
                case 5:
                    GetComponent<Image>().overrideSprite = CardAssunto6; //Mostra Sprite do deck sorteado
                    pergunta = ReadStringDeck(Cartas[ValorAssunto], NumeroCarta, GlobalVar.LanguageString); //envia a questão selecionada para uma string
                    TextoCarta.GetComponent<TextMeshProUGUI>().text = pergunta; //escreve o texto da carta para o objeto que exibe o texto
                    TextoCarta.SetActive(true);
                    break;
                default:
                    break;
            }
            titulo.GetComponent<Image>().overrideSprite = Titulo2; //muda o titulo da tela

            //Debug.Log(ValorAssunto);
            cliques++;

            BotaoResposta.SetActive(true);

            GetComponent<Button>().interactable = false;
        }

        else if (cliques == 1)
        {
            int NumeroCartaTemp = NumeroCarta + 1;

            if (NumeroCartaTemp % 2 != 0)
            {
                Debug.Log("A carta " + NumeroCartaTemp + " é verdadeira");
                titulo.GetComponent<Image>().overrideSprite = TituloTrue;
                CartaMoldura.GetComponent<Image>().overrideSprite = CardTrue;
            }
            else
            {
                Debug.Log("A carta " + NumeroCartaTemp + " é falsa");
                titulo.GetComponent<Image>().overrideSprite = TituloFalse;
                CartaMoldura.GetComponent<Image>().overrideSprite = CardFalse;
            }
            CartaMoldura.SetActive(true);

            BotaoDados.SetActive(true);
            //BotaoResposta.SetActive(true);

            cliques++;

            GetComponent<Button>().interactable = false;
        }

        //else if (cliques == 2)
        //{
        //    cliques = 0;
        //    Scene scene = SceneManager.GetActiveScene();
        //    SceneManager.LoadScene(scene.name);


        //}




    }

}

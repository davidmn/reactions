﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BotoesJogadores : MonoBehaviour
{
    public GameObject Jogadores2, Jogadores3, Jogadores4, Jogadores5, Jogadores6;
    public Sprite JogadorOn2_pt, JogadorOn2_en, JogadorOn2_es, JogadorOn2_fr, JogadorOn3_pt, JogadorOn3_en, JogadorOn3_es, JogadorOn3_fr, JogadorOn4_pt, JogadorOn4_en, JogadorOn4_es, JogadorOn4_fr, JogadorOn5_pt, JogadorOn5_en, JogadorOn5_es, JogadorOn5_fr, JogadorOn6_pt, JogadorOn6_en, JogadorOn6_es, JogadorOn6_fr;
    public Sprite JogadorOff2_pt, JogadorOff2_en, JogadorOff2_es, JogadorOff2_fr, JogadorOff3_pt, JogadorOff3_en, JogadorOff3_es, JogadorOff3_fr, JogadorOff4_pt, JogadorOff4_en, JogadorOff4_es, JogadorOff4_fr, JogadorOff5_pt, JogadorOff5_en, JogadorOff5_es, JogadorOff5_fr, JogadorOff6_pt, JogadorOff6_en, JogadorOff6_es, JogadorOff6_fr;
    Sprite JogadorOn2, JogadorOn3, JogadorOn4, JogadorOn5, JogadorOn6, JogadorOff2, JogadorOff3, JogadorOff4, JogadorOff5, JogadorOff6;
    public static GameObject objetoclicado;


    // Start is called before the first frame update
    void Start()
    {
        //Definir os sprites com as linguas certas
        switch (GlobalVar.Language)
        {
            case 0:
                JogadorOn2 = JogadorOn2_pt;
                JogadorOff2 = JogadorOff2_pt;
                JogadorOn3 = JogadorOn3_pt;
                JogadorOff3 = JogadorOff3_pt;
                JogadorOn4 = JogadorOn4_pt;
                JogadorOff4 = JogadorOff4_pt;
                JogadorOn5 = JogadorOn5_pt;
                JogadorOff5 = JogadorOff5_pt;
                JogadorOn6 = JogadorOn6_pt;
                JogadorOff6 = JogadorOff6_pt;
                break;
            case 1:
                JogadorOn2 = JogadorOn2_en;
                JogadorOff2 = JogadorOff2_en;
                JogadorOn3 = JogadorOn3_en;
                JogadorOff3 = JogadorOff3_en;
                JogadorOn4 = JogadorOn4_en;
                JogadorOff4 = JogadorOff4_en;
                JogadorOn5 = JogadorOn5_en;
                JogadorOff5 = JogadorOff5_en;
                JogadorOn6 = JogadorOn6_en;
                JogadorOff6 = JogadorOff6_en;
                break;
            case 2:
                JogadorOn2 = JogadorOn2_es;
                JogadorOff2 = JogadorOff2_es;
                JogadorOn3 = JogadorOn3_es;
                JogadorOff3 = JogadorOff3_es;
                JogadorOn4 = JogadorOn4_es;
                JogadorOff4 = JogadorOff4_es;
                JogadorOn5 = JogadorOn5_es;
                JogadorOff5 = JogadorOff5_es;
                JogadorOn6 = JogadorOn6_es;
                JogadorOff6 = JogadorOff6_es;
                break;
            case 3:
                JogadorOn2 = JogadorOn2_fr;
                JogadorOff2 = JogadorOff2_fr;
                JogadorOn3 = JogadorOn3_fr;
                JogadorOff3 = JogadorOff3_fr;
                JogadorOn4 = JogadorOn4_fr;
                JogadorOff4 = JogadorOff4_fr;
                JogadorOn5 = JogadorOn5_fr;
                JogadorOff5 = JogadorOff5_fr;
                JogadorOn6 = JogadorOn6_fr;
                JogadorOff6 = JogadorOff6_fr;
                break;
            default:
                JogadorOn2 = JogadorOn2_pt;
                JogadorOff2 = JogadorOff2_pt;
                JogadorOn3 = JogadorOn3_pt;
                JogadorOff3 = JogadorOff3_pt;
                JogadorOn4 = JogadorOn4_pt;
                JogadorOff4 = JogadorOff4_pt;
                JogadorOn5 = JogadorOn5_pt;
                JogadorOff5 = JogadorOff5_pt;
                JogadorOn6 = JogadorOn6_pt;
                JogadorOff6 = JogadorOff6_pt;
                break;
        }

        //atualizar os sprites selecionados
        Debug.Log(GlobalVar.NumJogadores);
        switch (GlobalVar.NumJogadores)
        {
            case 2:
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOn2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case 3:
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOn3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case 4:
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOn4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case 5:
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOn5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case 6:
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOn6;
                break;
            default:
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SelectJogador()
    {
        Debug.Log(objetoclicado.name);
        switch (objetoclicado.name)
        {
            case "Jogadores 2":
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOn2;
                GlobalVar.NumJogadores = 2;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case "Jogadores 3":
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOn3;
                GlobalVar.NumJogadores = 3;
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case "Jogadores 4":
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOn4;
                GlobalVar.NumJogadores = 4;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case "Jogadores 5":
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOn5;
                GlobalVar.NumJogadores = 5;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOff6;
                break;
            case "Jogadores 6":
                Jogadores6.GetComponent<Image>().overrideSprite = JogadorOn6;
                GlobalVar.NumJogadores = 6;
                Jogadores3.GetComponent<Image>().overrideSprite = JogadorOff3;
                Jogadores4.GetComponent<Image>().overrideSprite = JogadorOff4;
                Jogadores5.GetComponent<Image>().overrideSprite = JogadorOff5;
                Jogadores2.GetComponent<Image>().overrideSprite = JogadorOff2;
                break;
            default:
                break;
        }
    }
}
AVENGERS DECK 3

A003-001: Alkenes undergo addition when treated with bromine, while benzene is inert under the same conditions.
A003-002: Alkenes and benzene undergo addition when treated with bromine.
A003-003: In the presence of iron, an electrophilic aromatic substitution reaction is observed between benzene and bromine.
A003-004: In the presence of aluminium, an electrophilic aromatic substitution reaction is observed between benzene and bromine.
A003-005: Iron tribromide is a Lewis acid that interacts with Br2 and generates Br+, which is sufficiently electrophilic to be attacked by benzene.
A003-006:Iron tribromide is a Lewis acid that interacts with Br2 and generates Br2+, which is sufficiently electrophilic to be attacked by benzene.
A003-007: Electrophilic aromatic substitution involves two steps: (1) Formation of the sigma complex, or arenium ion. This step is endergonic. (2) Deprotonation, which restores aromaticity.
A003-008: Nucleophilic aromatic substitution involves two steps: (1) Formation of the sigma complex, or arenium ion. This step is endergonic. (2) Deprotonation, which restores aromaticity.
A003-009: Aluminum tribromide (AlBr3) is another common Lewis acid that can serve as a suitable alternative to FeBr3.
A003-010: Aluminum tribromide (AlBr3) is another common Lewis base that can serve as a suitable alternative to FeBr3.
A003-011: Chlorination of benzene is accomplished with a suitable Lewis acid, such as aluminum trichloride.
A003-012: Chlorination of benzene is accomplished with a suitable Lewis acid, such as aluminum tribromide.
A003-013: Sulfur trioxide (SO3) is a very powerful electrophile that is present in fuming sulfuric acid. Benzene reacts with SO3 in a reversible process called sulfonation.
A003-014: Sulfur trioxide (SO3) is a very powerful electrophile that is present in diluted sulfuric acid. Benzene reacts with SO3 in a reversible process called sulfonation.
A003-015: A mixture of sulfuric acid and nitric acid produces a small amount of nitronium ion (NO2+). Benzene reacts with the nitronium ion in a process called nitration.
A003-016: A mixture of sulfuric acid and nitric acid produces a small amount of nitronium ion (NO+). Benzene reacts with the nitronium ion in a process called nitration.
A003-017: A nitro group can be reduced to an amino group, providing a two-step method for installing an amino group.
A003-018: A nitro group can be reduced to an imine group, providing a two-step method for installing an amino group.
A003-019: Friedel-Crafts alkylation enables the installation of an alkyl group on an aromatic ring.
A003-020: Friedel-Crafts alkylation enables the installation of an acyl group on an aromatic ring.
A003-021: In the presence of a Lewis acid, an alkyl halide is converted into a carbocation, which can be attacked by benzene in an electrophilic aromatic substitution.
A003-022: In the presence of a Lewis acid, an acyl halide is converted into a carbocation, which can be attacked by benzene in an electrophilic aromatic substitution.
A003-023: A Friedel-Crafts alkylation is only efficient in cases where the carbocation cannot rearrange.
A003-024: A Friedel-Crafts alkylation is only efficient in cases where the carbocation can rearrange.
A003-025: Regarding a Friedel-Crafts alkylation, when choosing an alkyl halide, the carbon atom connected to the halogen must be sp3 hybridized.
A003-026: Regarding a Friedel-Crafts alkylation, when choosing an alkyl halide, the carbon atom connected to the halogen must be sp2 hybridized.
A003-027: Polyalkylations in aromatic compounds are common and can generally be avoided A003-028: Polynitrations in aromatic compounds are common and can generally be avoided by controlling the reaction conditions.
A003-029: Friedel-Crafts acylation enables the installation of an acyl group on an aromatic A003-030: Friedel-Crafts acylation enables the installation of an alkyl group on an aromatic ring.
A003-031: When treated with a Lewis acid, an acyl chloride will generate an acylium ion, which is resonance stabilized and not susceptible to carbocation rearrangements.
A003-032: When treated with a Lewis acid, an acyl chloride will generate an acylium ion, which is not resonance stabilized and is susceptible to carbocation rearrangements.
A003-033: When a Friedel-Crafts acylation is followed by a Clemmensen reduction, the net A003-034: When a Friedel-Crafts acylation is followed by a Clemmensen reduction, the net result is the installation of an acyl group.
A003-035: Polyacylation is not observed, because introduction of an acyl group deactivates the ring toward further acylation.
A003-036: Polyacylation is observed, because introduction of an acyl group activates the ring toward further acylation.
A003-037: An aromatic ring is activated by a methyl group, which is an ortho-para director.
A003-038: An aromatic ring is deactivated by a methyl group, which is an ortho-para director.
A003-039: An aromatic ring is even more highly activated by a methoxy group, which is also an ortho- para director.
A003-040: An aromatic ring is even more highly activated by a methoxy group, which is also a meta director.
A003-041: A nitro group deactivates an aromatic ring and is a meta director. Most deactivators are meta directors.
A003-042: A nitro group deactivates an aromatic ring and is a meta director. However, most deactivators are ortho-para directors.
A003-043: Halogens are an exception in that they are deactivators but are ortho-para directors.
A003-044: Halogens are activators and ortho-para directors.
A003-045: Strong activators are characterized by the presence of alone pair immediately adjacent to the aromatic ring.
A003-046: Weak activators are characterized by the presence of alone pair immediately adjacent to the aromatic ring.
A003-047: Moderate activators exhibit a lone pair that is already delocalized outside of the ring. Alkoxy groups are an exception and are moderate activators.
A003-048: Strong activators exhibit a lone pair that is already delocalized outside of the ring. Alkoxy groups are an exception and are moderate activators.
A003-049: Alkyl groups are weak activators.
A003-050: Acyl groups are weak activators.
A003-051: Halogens are weak deactivators.
A003-052: Halogens are weak activators.
A003-053: Moderate deactivators are groups that exhibit a ? bond to an electronegative atom, where the ? bond is conjugated with the aromatic ring.
A003-054: Moderate activators are groups that exhibit a ? bond to an electronegative atom, where the ? bond is conjugated with the aromatic ring.
A003-055: Strong deactivators are powerfully electron withdrawing, either by resonance or induction.
A003-056: Strong activators are powerfully electron withdrawing, either by resonance or induction.
A003-057: When multiple substituents are present, the more powerful activating group dominates the directing effects.
A003-058: When multiple substituents are present, the more powerful deactivating group dominates the directing effects.
A003-059: Steric effects often play an important role in determining product distribution.
A003-060: Steric effects often do not play an important role in determining product distribution.
A003-061: A blocking group can be used to control the regiochemical outcome of an electrophilic aromatic substitution.
A003-062: A nitro group can be used to control the regiochemical outcome of an electrophilic aromatic substitution.
A003-063: Proposing a synthesis for a disubstituted benzene ring requires a careful analysis of directing effects to determine which group should be installed first.
A003-064: Proposing a synthesis for a disubstituted benzene ring does not require a careful analysis of directing effects to determine which group should be installed first.
A003-065: In a nucleophilic aromatic substitution reaction, the aromatic ring is attacked by a nucleophile.
A003-066: In a nucleophilic aromatic substitution reaction, the aromatic ring is attacked by an electrophile.
A003-067: In a nucleophilic aromatic substitution reaction, the ring must contain a powerful electron-withdrawing group (typically a nitro group).
A003-068: In a nucleophilic aromatic substitution reaction, the ring must contain a powerful activating group.
A003-069: In a nucleophilic aromatic substitution reaction, the ring must contain a leaving group.
A003-070: In an electrophilic aromatic substitution reaction, the ring must contain a leaving group.
A003-071: In a nucleophilic aromatic substitution reaction, the leaving group must be either ortho or para to the electron-withdrawing group.
A003-072: In an electrophilic aromatic substitution reaction, the leaving group must be either ortho or para to the electron-withdrawing group.
A003-073: Nucleophilic aromatic substitution involves two steps: (1) Formation of a Meisenheimer complex. (2) Loss of a leaving group to restore aromaticity.
A003-074: Electrophilic aromatic substitution involves two steps: (1) Formation of a Meisenheimer complex. (2) Loss of a leaving group to restore aromaticity.
A003-075: An elimination-addition reaction occurs via a benzyne intermediate. Evidence for this mechanism comes from isotopic labeling experiments as well as a trapping experiment.
A003-076: An addition-elimination reaction occurs via a benzyne intermediate. Evidence for this mechanism comes from isotopic labeling experiments as well as a trapping experiment.
A003-077: The three mechanisms for aromatic substitution differ in (1) the intermediate, (2) the leaving group, and (3) substituent effects.
A003-078: The three mechanisms for aromatic substitution differ in (1) the intermediate, (2) the nucleophile, and (3) substituent effects.
A003-079: During bromination of an alkene, bromine functions as an electrophile. It approaches the ? electron cloud of the alkene, Br2 becomes temporarily polarized, rendering one of the bromine atoms electrophilic (?+).
A003-080: During bromination of an alkene, bromine functions as a nucleophile. It approaches the ? electron cloud of the alkene, Br2 becomes temporarily polarized, rendering one of the bromine atoms nucleophilic (?-).
A003-081: The bromine atom becomes sufficiently electrophilic to react with the alkene but is not sufficiently electrophilic to react with benzene.
A003-081: The bromine atom becomes sufficiently nucleophilic to react with the alkene but is not sufficiently nucleophilic to react with benzene.
A003-083: Iron tribromide, a Lewis acid, is the real catalyst in the reaction between benzene and bromine.
A003-084: Boron tribromide, a Lewis acid, is the real catalyst in the reaction between benzene and bromine.
A003-085: When benzene is treated with fuming sulfuric acid, a sulfonation reaction occurs and benzenesulfonic acid is obtained.
A003-086: When benzene is treated with diluted sulfuric acid, a sulfonation reaction occurs and benzenesulfonic acid is obtained.
A003-087: The reaction between benzene and SO3 is highly sensitive to the concentrations of the reagents and is, therefore, reversible.
A003-088: The reaction between benzene and SO3 is highly sensitive to the concentrations of the reagents and is not reversible.
A003-089: When benzene is treated with a mixture of nitric acid and sulfuric acid, a nitration reaction occurs in which nitrobenzene is formed.
A003-090: When benzene is treated with a mixture of nitric acid and sulfuric acid, a nitration reaction occurs in which dinitrobenzene is formed.
A003-091: The nitration reaction proceeds via an electrophilic aromatic substitution in which a nitronium ion (NO2+) is believed to be the electrophile. This strong electrophile is formed from the acid-base reaction that takes place between HNO3 and H2SO4.
A003-092: The nitration reaction proceeds via an electrophilic aromatic substitution in which a nitronium ion (NO+) is believed to be the electrophile. This strong electrophile is formed from the acid-base reaction that takes place between HNO3 and H2SO4.
A003-093: In a nitration reaction, the nitric acid functions as a base to accept a proton from sulfuric acid, followed by loss of water to produce a nitronium ion.
A003-094: In a nitration reaction, the sulfuric acid functions as a base to accept a proton from nitric acid, followed by loss of water to produce a nitronium ion.
A003-095: Installation of an alkyl group activates the ring toward further alkylation.
A003-096: Installation of an alkyl group deactivates the ring toward further alkylation.
A003-097: When a Friedel-Crafts acylation is followed by a Clemmensen reduction, the net result is the installation of an alkyl group.
A003-098: When a Friedel-Crafts acylation is followed by a Clemmensen reduction, the net result is the installation of an acyl group.
A003-099: Toluene undergoes nitration approximately 25 times faster than benzene. In other words, the methyl group is said to activate the aromatic ring.
A003-100: Toluene undergoes nitration approximately 25 times slower than benzene. In other words, the methyl group is said to deactivate the aromatic ring.
A003-101:




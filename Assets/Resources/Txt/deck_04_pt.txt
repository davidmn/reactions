AVENGERS DECK 4

A004-001: Ao preparar um álcool por meio de uma reação de substituição, os substratos primários exigirão condições de SN2, enquanto substratos terciários exigirão condições SN1.

A004-002: Ao preparar um álcool por meio de uma reação de substituição, os substratos primários exigirão condições de SN1, enquanto substratos terciários exigirão condições SN2.

A004-003: As reações de adição que produzirão álcoois incluem hidratação catalisada por ácido, oximercuração-desmercuração e oxidação por hidroboração.

A004-004: As reações de adição que produzirão álcoois incluem hidratação catalisada por base, oximercuração-desmercuração e oxidação por hidroboração.

A004-005: Os álcoois podem ser formados tratando um grupo carbonila ligação (C=O) com um agente redutor. A reação resultante envolve uma diminuição no estado de oxidação e é chamado de redução.

A004-006: Os álcoois podem ser formados tratando um grupo carbonila ligação (C=O) com um agente redutor. A reação resultante envolve um aumento no estado de oxidação e é chamado de redução.

A004-007: O LiAlH4 é mais reativo que o NaBH4. O LiAlH4 reduzirá os ácidos carboxílicos e ésteres, enquanto o NaBH4 não.

A004-008: O NaBH4 é mais reativo que o LiAlH4. O NaBH4 reduzirá os ácidos carboxílicos e ésteres, enquanto o LiAlH4 não.

A004-009: Dióis são compostos com dois grupos hidroxilas.

A004-010: Trióis são compostos com dois grupos hidroxilas.

A004-011: Os dióis podem ser preparados a partir de dicetonas, via redução, usando um agente redutor.

A004-012: Os dióis podem ser preparados a partir de dicetonas, via oxidação, usando um agente oxidante.

A004-013: Os dióis também podem ser feitos via diidroxilação syn ou diidroxilação anti de um alceno.

A004-014: Os dióis também podem ser feitos via diidroxilação syn ou diidroxilação anti de um alcino.

A004-015: Os reagentes de Grignard são nucleófilos de carbono que são capazes de atacar uma ampla gama de eletrófilos, incluindo o grupo carbonila de cetonas ou aldeídos, para produzir um álcool.

A004-016: Os reagentes de Grignard são nucleófilos de carbono que são capazes de atacar uma ampla gama de nucleófilos, incluindo o grupo carbonila de cetonas ou aldeídos, para produzir um álcool.

A004-017: Os reagentes de Grignard também reagem com ésteres para produzir álcoois com a introdução de dois grupos R.

A004-018: Os reagentes de Grignard também reagem com ésteres para produzir álcoois com a introdução de um grupo R.

A004-019: Álcoois terciários sofrerão uma reação SN1 quando tratados com um halogeneto de hidrogênio.

A004-020: Álcoois terciários sofrerão uma reação SN2 quando tratados com um halogeneto de hidrogênio.

A004-021: Álcoois primários e secundários sofrerão um processo SN2 quando tratados com HX, SOCl2 ou PBr3 ou quando a hidroxila grupo é convertida em um grupo tosilato, seguido por ataque nucleofílico.

A004-022: Álcoois primários e secundários sofrerão um processo SN1 quando tratados com HX, SOCl2 ou PBr3 ou quando a hidroxila grupo é convertida em um grupo tosilato, seguido por ataque nucleofílico.

A004-023: Álcoois terciários sofrem eliminação E1 quando tratados com ácido sulfúrico.

A004-024: Álcoois terciários sofrem eliminação E2 quando tratados com ácido sulfúrico.

A004-025: Para um processo E2, o grupo hidroxila é convertido em um tosilato ou um halogeneto de alquila.

A004-026: Para um processo E2, o grupo hidroxila é primeiro convertido em um tosilato ou um halogeneto de acila.

A004-027: Álcoois primários podem sofrer oxidação duas vezes para dar um ácido carboxílico.

A004-028: Álcoois secundários podem sofrer oxidação duas vezes para dar um ácido carboxílico.

A004-029: Álcoois secundários podem sofrer oxidação duas vezes para dar uma cetona.

A004-030: Álcoois terciários podem sofrer oxidação duas vezes para dar uma cetona.

A004-031: Álcoois terciários não sofrem oxidação.

A004-032: Álcoois terciários podem sofrer oxidação para gerar ésteres.

A004-033: O reagente oxidante mais comum é o ácido crômico (H2CrO4), que pode ser preparado a partir do trióxido de cromo (CrO3) ou do dicromato de sódio (Na2Cr2O7) em meio aquoso ácido.

A004-034: O reagente oxidante mais comum é o ácido crômico (H2CrO4), que pode ser preparado a partir do trióxido de cromo (CrO3) ou do dicromato de sódio (Na2Cr2O7) em meio aquoso básico.

A004-035: PCC é usado para converter um álcool primário em um aldeído.

A004-036: PCC é usado para converter um álcool primário em um ácido carboxílico.

A004-037: Os fenóis sofrem oxidação para gerar quinonas.

A004-038: Os fenóis sofrem redução para gerar quinonas.

A004-039: Uma base forte pode ser usada para desprotonar o álcool. Uma base comumente usada é o hidreto de sódio (NaH).

A004-040: Uma base forte pode ser usada para desprotonar o álcool. Uma base comumente usada é o hidróxido de sódio (NaOH).

A004-041: Li, Na ou K reagem com um álcool para liberar gás hidrogênio, produzindo o íon
alcóxido.

A004-042: Li, Na ou K reagem com um álcool para liberar gás hidrogênio, produzindo o íon
fenóxido.

A004-043: Os álcoois podem ser preparados por reações de substituição nucleofílica em que um grupo abandonador é substituído por um grupo hidroxila.

A004-044: Os álcoois podem ser preparados por reações de substituição aromática em que um grupo abandonador é substituído por um grupo hidroxila.

A004-045: O hidreto de alumínio e lítio é um reagente redutor forte. Reage violentamente com a água e, portanto, um solvente prótico não pode estar presente junto com o LiAlH4 no balão de reação.

A004-046: O hidreto de alumínio e lítio é um reagente redutor fraco. Reage lentamente com a água e, portanto, um solvente prótico pode estar presente junto com o LiAlH4 no balão de reação.

A004-047: Tanto o NaBH4 como o LiAlH4 reduzirão as cetonas ou os aldeídos. Estes agentes de fornecimento de hidreto oferecem uma vantagem significativa em relação à hidrogenação catalítica, na medida em que podem reduzir seletivamente um grupo carbonila na presença de uma ligação C=C.

A004-048: Tanto o NaBH4 como o LiAlH4 reduzirão as cetonas ou os aldeídos. Estes agentes de fornecimento de hidreto oferecem uma vantagem significativa em relação à hidrogenação catalítica, na medida em que podem reduzir seletivamente uma ligação C=C na presença de um grupo carbonila.

A004-049: O fenol é preparado industrialmente através de um processo de múltiplas etapas envolvendo a formação e oxidação do cumeno.

A004-050: O fenol é preparado industrialmente através de um processo de múltiplas etapas envolvendo a formação e oxidação da anilina.

A004-051: Os éteres podem ser prontamente preparados a partir da reação entre um íon alcóxido e um haleto de alquila, um processo chamado de síntese de Williamson.

A004-052: Os éteres podem ser prontamente preparados a partir da reação entre um íon alcóxido e um haleto de acila, um processo chamado de síntese de Williamson.

A004-053: Uma síntese de éter de Williamson funciona melhor para haletos de metila ou haletos de alquila primários.  

A004-054: Uma síntese de éter de Williamson funciona melhor para haletos de alquila primários ou secundários.  

A004-055: Os halogenetos de alquila secundários são significativamente menos eficientes, e os halogenetos de alquila terciários não podem utilizados numa síntese de éter de Williamson.

A004-056: Os halogenetos de alquila primários são significativamente menos eficientes, e os halogenetos de alquila terciários não podem ser utilizados numa síntese de éter de Williamson.

A004-057: Os éteres podem ser preparados a partir de alcenos através alcoximercuração-desmercurização, o que resulta numa adição de Markovnikov de RO e H através de um alceno.

A004-058: Os éteres podem ser preparados a partir de alcenos através de alcoximercuração-desmercurização, o que resulta numa adição de Zaytsev de RO e H a um alceno.

A004-059: Quando tratado com um ácido forte, um éter sofrerá clivagem ácida em que é convertido em dois haletos de alquila.

A004-060: Quando tratado com uma base forte, um éter sofrerá clivagem ácida em que é convertido em dois haletos de alquila.

A004-061: Quando um éter fenílico é clivado sob condições ácidas, os produtos são fenol e um haleto de alquila.  

A004-062: Quando um éter fenílico é clivado sob condições ácidas, os produtos são dois haletos de alquila.  
A004-063: Um éter cíclico de três membros é chamado oxirano. Possui tensão anelar significante e é, portanto, mais reativo do que outros éteres.

A004-064: Um éter cíclico de três membros é chamado oxano. Possui tensão anelar significante e é, portanto, mais reativo do que outros éteres.

A004-065: Os alcenos podem ser convertidos em epóxidos por tratamento com peroxi-ácidos ou através da formação de haloidrina e, subsequente, ciclização intramolecular. Ambos os procedimentos são estereoespecíficos.

A004-066: Os alcenos podem ser convertidos em epóxidos por tratamento com peroxi-ácidos ou através da formação de haloidrina e, subsequente, ciclização intramolecular. Ambos os procedimentos são estereoseletivos.

A004-067: Substituintes que são cis entre si no alceno de partida permanecem cis entre si no epóxido, e substituintes que  são trans no alceno de partida permanecem trans no epóxido.

A004-068: Substituintes que são cis entre si no alceno de partida tornam-se trans no epóxido.

A004-069: Os catalisadores quirais podem ser usados para promover a epoxidação enantiosseletiva de
álcoois alílicos.

A004-070: Os catalisadores aquirais podem ser usados para promover a epoxidação enantiosseletiva de álcoois alílicos.

A004-071: Em uma epoxidação assimétrica de Sharpless, o catalisador favorece a produção de um enantiômero sobre o outro, levando a um excesso enantiomérico.

A004-072: Em uma epoxidação assimétrica de Sharpless, o catalisador favorece a produção de um enantiômero sobre o outro, levando a uma mistura racêmica.

A004-073: Os epóxidos sofrerão reações de abertura do anel (1) em condições envolvendo um nucleófilo forte ou (2) sob condições catalisadas por ácido.

A004-074: Os epóxidos sofrerão reações de abertura do anel (1) em condições envolvendo um eletrófilo forte ou (2) sob condições catalisadas por ácido.

A004-075: Quando um nucleófilo forte é usado para abrir um epóxido, o nucleófilo ataca a posição menos substituída (menos impedida).

A004-076: Quando um nucleófilo forte é usado para abrir um epóxido, o nucleófilo ataca a posição mais substituída (mais impedida).

A004-077: Sob condições catalisadas por ácido, o resultado regioquímico é dependente da natureza do epóxido e é explicado em termos de uma competição entre efeitos eletrônicos e efeitos estéricos.

A004-078: Sob condições catalisadas por base, o resultado regioquímico é dependente da natureza do epóxido e é explicado em termos de uma competição entre efeitos eletrônicos e efeitos estéricos.

A004-079: O resultado estereoquímico envolve a inversão da configuração sob todas as condições.

A004-080: O resultado estereoquímico envolve a retenção da configuração sob todas as condições.

A004-081: Análogos de enxofre de álcoois contêm um grupo SH em vez de um OH grupo e são chamados tióis.

A004-082: Análogos de enxofre de álcoois contêm um grupo SO3H em vez de um OH grupo e são chamados tióis.

A004-083: A abertura de um epóxido produz um composto com dois grupos funcionais em átomos de carbono adjacentes. Sempre que você vê dois grupos funcionais adjacentes, você deve pensar em epóxidos.

A004-084: A abertura de um epóxido produz um composto com dois grupos funcionais em átomos de carbono adjacentes. Sempre que você vê dois grupos funcionais adjacentes, você deve pensar em alcóxidos.

A004-085: Quando um reagente de Grignard reage com um epóxido, uma ligação C-C é formada. Esta reação pode ser usada para introduzir uma cadeia de átomos de carbono que possui um grupo funcional incorporado no segundo átomo de carbono.

A004-086: Quando um reagente de Grignard reage com um epóxido, uma ligação C=C é formada. Esta reação pode ser usada para introduzir uma cadeia de átomos de carbono que possui um grupo funcional incorporado no segundo átomo de carbono.

A004-087: Éteres são compostos que exibem um átomo de oxigênio ligado a dois grupos R, onde cada grupo R pode ser um grupo alquila, arila ou vinila.

A004-088: Ésteres são compostos que exibem um átomo de oxigênio ligado a dois grupos R, onde cada grupo R pode ser um grupo alquila, arila ou vinila.

A004-089: O éter dietílico é preparado industrialmente através da desidratação do etanol catalisada por ácido.

A004-090: O éter dietílico é preparado industrialmente através da desidratação do etanol catalisada por base.

A004-091: Os éteres, geralmente, não são reativos em condições básicas ou levemente ácidas. Como resultado, eles são a escolha ideal como solventes para muitas reações.

A004-092: Os éteres são geralmente não reativos sob condições básicas ou levemente ácidas. Como resultado, eles são uma escolha ideal como solventes para muitas reações.

A004-093: Tanto o HI como o HBr podem ser usados para clivar os éteres. O HCl é menos eficiente e a HF não causa clivagem de éteres.

A004-094: Tanto o HI como o HBr podem ser usados para clivar os éteres. O HBr é menos eficiente e a HI não causa clivagem de éteres

A004-095: Os alcenos podem ser convertidos em epóxidos após o tratamento com peróxidos.

A004-096: Os alcinos podem ser convertidos em epóxidos após o tratamento com peróxidos.

A004-097: Os alcenos podem ser convertidos em haloidrinas quando tratados com halogênio na presença de água.

A004-098: Os alcenos podem ser convertidos em haloidrinas quando tratados com halogênio na presença de álcool. 

A004-099: Haloidrinas podem ser convertidas em epóxidos após o tratamento com uma base forte.

A004-100: Haloidrinas podem ser convertidas em epóxidos após o tratamento com um ácido forte.
A004-101:





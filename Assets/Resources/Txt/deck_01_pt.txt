AVENGERS DECK 1

A001-001: As reações de adição são caracterizadas pela adição de dois grupos às ligações duplas ou triplas.
A001-002: As reações de adição são caracterizadas pela adição de um grupo às ligações duplas ou triplas.
A001-003: As reações de adição são termodinamicamente favoráveis em baixas temperaturas e desfavorecidas em altas temperaturas.
A001-004: As reações de adição são termodinamicamente favoráveis em altas temperatura e desfavorecidas em baixas temperaturas.
A001-005: As reações de hidroalogenação são caracterizadas pela adição de H e X à uma ligação π.
A001-006: As reações de hidroalogenação são caracterizadas pela adição de H e X à uma ligação sigma.
A001-007: Para alcenos assimétricos, o posicionamento do halogênio (adição de HX) representa uma questão de regioquímica.
A001-008: Para alcenos assimétricos, posicionamento do halogênio (adição de HX) representa uma questão de estereoquímica.
A001-009: As reações de hidroalogenação são regiosseletivas, porque o halogênio é geralmente colocado na posição mais substituída - Adição de Markovnikov.
A001-010: As reações de hidroalogenação são regiosseletivas, porque o halogênio é geralmente colocado na posição menos substituída - Adição de Markovnikov.
A001-011: Na presença de peróxidos, a adição de HBr ocorre através de uma adição anti-Markovnikov.
A001-012: Na presença de peróxidos, a adição de HBr ocorre através de uma adição Markovnikov.
A001-013: A regiosseletividade de uma reação de adição iônica é determinada pela preferência de que a reação prossiga através do intermediário de carbocátion mais estável.
A001-014: A regiosseletividade de uma reação de adição iônica é determinada pela preferência de que a reação prossiga através do intermediário de carbocátion menos estável.
A001-015: Quando um novo centro de quiralidade é formado em uma reação de adição, uma mistura racêmica de enantiômeros é obtida.
A001-016: Quando um novo centro de quiralidade é formado em uma reação de adição, um único enantiômero é obtido.
A001-017: As reações de hidroalogenação são apenas efetivas quando os rearranjos de carbocátion não são possíveis.
A001-018: As reações de hidroalogenação são apenas efetivas quando os rearranjos de carbocátion  são possíveis.
A001-019: A adição de água (H e OH) à uma ligação dupla é chamada hidratação.
A001-020: A adição de água (H e OH) à uma ligação dupla é chamada hidroalogenação.
A001-021: A adição de água na presença de um ácido é chamada de hidratação catalisada por ácido, a qual geralmente ocorre via uma adição de Markovnikov.
A001-022: A adição de água na presença de um ácido é chamada de hidratação catalisada por ácido, a qual geralmente ocorre via uma adição de anti-Markovnikov.
A001-023: A hidratação catalisada por ácido procede através de um intermediário carbocátion, que é atacado pela água para produzir um íon oxônio, seguido por uma desprotonação.
A001-024: A hidratação catalisada por ácido procede através de um intermediário carbocátion, que é atacado pela água para produzir um íon hidrônio, seguido por uma desprotonação.
A001-025: A hidratação catalisada por ácido é ineficaz quando os rearranjos de carbocátions são possíveis.
A001-026: A hidratação catalisada por ácido é eficaz quando os rearranjos de carbocátions são possíveis.
A001-027: Ácido diluído favorece a formação do álcool, enquanto o ácido concentrado favorece o alceno.
A001-028: Ácido diluído favorece a formação do alceno, enquanto o ácido concentrado favorece o álcool.
A001-029: Ao gerar um novo centro de quiralidade em uma reação de adição, uma mistura racêmica de enantiômeros é esperada.
A001-030: Ao gerar um novo centro de quiralidade em uma reação de adição, uma mistura racêmica de diastereoisômeros é esperado.
A001-031: Hidroboração-oxidação pode ser usada para conseguir uma adição de água anti-Markovnikov a um alceno. A reação é estereospecífica e prossegue através de uma adição syn.
A001-032: Hidroboração-oxidação pode ser usada para conseguir uma adição de água Markovnikov a um alceno. A reação é estereospecífica e prossegue através de uma adição syn.
A001-033: A hidroboração prossegue através de um processo concertado, no qual o borano é atacado por uma ligação π, desencadeando uma transferência de hidreto.
A001-034: A hidroboração prossegue através de um processo concertado, no qual o borano ataca uma ligação π, desencadeando uma transferência de hidreto.
A001-035: A hidrogenação catalítica envolve a adição de H2 a um alceno na presença de um catalisador metálico.
A001-036: A hidrogenação catalítica envolve a adição de H2 a um alceno na presença de um catalisador metálico, como carbono.
A001-037: Na presença de um ácido, o grupo carbonila é protonado para formar um eletrófilo muito poderoso.
A001-038: Na presença de um ácido, o grupo carbonila é protonado para formar um nucleófilo muito poderoso.
A001-039: A hidrogenação catalítica procede através de uma adição syn.
A001-040: A hidrogenação catalítica procede através de uma adição anti.
A001-041: Catalisadores heterogêneos não são solúveis no meio reacional, enquanto os catalisadores homogêneos são.
A001-042: Catalisadores heterogêneos são solúveis no meio reacional, enquanto os catalisadores homogêneos não são.
A001-043: Halogenação envolve a adição de X2 (Br2 ou Cl2) a um alceno.
A001-044: Halogenação envolve a adição de X2 (Br2, Cl2 ou I2) a um alceno.
A001-045: A bromação ocorre através de um intermediário em ponte, denominado íon bromônio, que é aberto por um processo SN2 que produz uma adição anti.
A001-046: A bromação ocorre através de um intermediário em ponte, denominado íon bromônio, que é aberto por um processo SN2 que produz uma adição syn.
A001-047: A bromação ocorre através de um intermediário em ponte, denominado íon bromônio. No entanto, na presença de água, o produto é uma bromoidrina, e a reação é chamada formação de haloidrina.
A001-048: A bromação prossegue através de um intermediário em ponte, denominado íon bromônio. No entanto, na presença de água, o produto é uma bromoidrina e a reação é chamada formação de cianoidrina.
A001-049: As reações de diidroxilação são caracterizadas pela adição de dois grupos OH a um alceno.
A001-050: As reações de diidroxilação são caracterizadas pela adição de dois grupos OH a um alcano.
A001-051: Um procedimento de duas etapas para uma diidroxilação anti envolve a conversão de um alceno em um epóxido, seguido pela abertura do anel catalisada por ácido.
A001-052: Um procedimento de três etapas para uma diidroxilação anti envolve a conversão de um alceno em um epóxido, seguido pela abertura do anel catalisada por ácido.
A001-053: A diidroxilação syn pode ser conseguida com tetróxido de ósmio ou permanganato de potássio.
A001-054: A diidroxilação anti pode ser conseguida com tetróxido de ósmio ou permanganato de potássio.
A001-055: A ozonólise pode ser usada para clivar uma ligação dupla e produzir duas carbonilas.
A001-056: A ozonólise pode ser usada para clivar uma ligação tripla e produzir duas carbonilas.
A001-057: A hidrogenação catalítica de um alcino produz um alcano.
A001-058: A hidrogenação catalítica de um alcino produz um alceno.
A001-059: A hidrogenação catalítica na presença de um catalisador envenenado (catalisador de Lindlar ou Ni2B) produz um alceno cis.
A001-060: A hidrogenação catalítica na presença de um catalisador envenenado (catalisador de Lindlar ou Ni2B) produz um alceno trans.
A001-061: Uma redução por metal dissolvido converterá um alcino em um alceno trans.
A001-062: Uma redução por metal dissolvido converterá um alcino em um alceno cis.
A001-063: Os alcinos reagem com o HX através de uma adição syn-Markovnikov.
A001-064: Os alcinos reagem com o HX através de uma adição  anti-Markovnikov.
A001-065: Um possível mecanismo para a hidroalogenação de alcinos envolve um carbocátion vinílico.
A001-066: Um possível mecanismo para a hidroalogenação de alcinos envolve um carbocátion alílico.
A001-067: A adição de HX a alcinos provavelmente ocorre através de uma variedade de vias mecanísticas, todas ocorrendo no mesmo tempo e competindo umas com as outras.
A001-068: A adição de HX a alcenos provavelmente ocorre através de uma variedade de vias mecanísticas, todas ocorrendo no mesmo tempo sem competir umas com as outras.
A001-069: O tratamento de um alcino terminal com HBr e peróxidos gera uma adição anti-Markovnikov de HBr.
A001-070: O tratamento de um alcino terminal com HBr e peróxidos gera uma adição Markovnikov de HBr.
A001-071: A hidratação de alcinos é catalisada por sulfato de mercúrio (HgSO4) para produzir um enol que não pode ser isolado porque é rapidamente convertido em uma cetona.
A001-072: A hidratação de alcinos é catalisada por sulfato de mercúrio (HgSO4) para produzir um enol que pode ser isolado porque é lentamente convertido em uma cetona.
A001-073: Enóis e cetonas são tautômeros, isômeros constitucionais que rapidamente se convertem através da migração de um próton.
A001-074: Enóis e cetonas são tautômeros, isômeros constitucionais que rapidamente se convertem através da migração de dois prótons.
A001-075: A interconversão entre um enol e uma cetona é chamada tautomerização ceto-enólica, e é catalisada por quantidades mínimas de ácido ou base.
A001-076: A interconversão entre um enol e uma cetona é chamada racemização ceto-enólica, e é catalisada por quantidades mínimas de ácido ou base.
A001-077: Os alcinos podem sofrer halogenação para formar tetraalogenetos.
A001-078: Os alcinos podem sofrer halogenação para formar dialogenetos.
A001-079: Quando tratados com ozônio seguido de água, alcinos internos sofrem clivagem oxidativa para produzir ácidos carboxílicos.
A001-080: Quando tratados com ozônio seguido de água, alcinos internos sofrem clivagem oxidativa para produzir cetonas.
A001-081: Quando um alcino terminal sofre clivagem oxidativa, o lado terminal é convertido em dióxido de carbono.
A001-082: Quando um alcino terminal sofre clivagem oxidativa, o lado terminal é convertido em ácidos carboxílicos.
A001-083: Os íons alcinídeos sofrem alquilação quando tratados com um halogeneto de alquila (metila ou primário).
A001-084: Os íons alcinídeos sofrem alquilação quando tratados com um halogeneto de alquila (primário ou secundário).
A001-085: Um alceno pode ser convertido em um alcino via bromação seguido de eliminação com excesso de NaNH2.
A001-086: Um alcino pode ser convertido em um alceno via bromação seguido de eliminação com excesso de NaNH2.
A001-087: Os alcinos sofrem muitas das mesmas reações de adição que os alcenos. Por exemplo, alcinos irão sofrer hidrogenação catalítica tal como os alcenos.
A001-088: Os alcinos sofrem muitas das mesmas reações de adição que os alcenos. Por exemplo, alcinos irão sofrer hidrogenação catalítica tal como os alcanos.
A001-089: Nas hidrogenações catalíticas, um alcino consome dois equivalentes de hidrogênio molecular produzindo um alcano.
A001-090: Nas hidrogenações catalíticas, um alcino consome um equivalente de hidrogênio molecular produzindo um alceno.
A001-091: A hidrogenação catalítica de alcinos com um catalisador parcialmente desativado, chamado de catalisador envenenado, converte um alcino em um alceno cis.
A001-092: Existem muitos catalisadores envenenados. Dois exemplos comuns são: o catalisador de Lindlar e o complexo de níquel-boro (Ni2B), que é frequentemente chamado de catalisador P-2.
A001-093: Um catalisador envenenado catalisará a conversão de um alcino em um alceno cis, mas não catalisará a redução subsequente para formar o alcano. Portanto, um catalisador envenenado pode ser usado para converter um alcino em um alceno cis.
A001-094: Os alcinos podem ser reduzidos a alcenos trans através de uma reação completamente diferente chamada dissolução redutiva de metal.
A001-095: Em uma redução de alcino com Na/NH3, a natureza do intermediário (ânion radical) explica a preferência estereoquímica pela formação de um alceno trans.
A001-096: Hidroboração-oxidação de alcinos é um processo concertado que dá uma adição Markovnikov.
A001-097: Para evitar uma segunda adição em alcinos, emprega-se um dialquilborano (R2BH) em vez de BH3. Os dois grupos alquilas fornecem impedimento estérico que impede a segunda adição. Dois dialquilboranos vulgarmente utilizados são disiamilborano e 9-BBN.
A001-098: Alcinos têm duas ligações π e podem adicionar dois equivalentes do halogênio para formar um dialogeneto.
A001-099: Quando tratados com ozônio seguido de água, os alcinos sofrem clivagem oxidativa para produzir ácidos carboxílicos.
A001-100: Quando um alcino terminal sofre clivagem oxidativa, o lado terminal é convertido em móxido de carbono.
A001-101:



